Rails.application.routes.draw do
  use_doorkeeper
  post '/users/send_code', to: 'code#send_code'

  devise_for :users, controllers: { sessions: 'users/sessions' } do
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "application#home"
end
