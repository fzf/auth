FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test#{n}@example.com" }
    phone { Faker::Number.number(10) }
    password 'password'
  end

  factory :doorkeeper_application, class: 'Doorkeeper::Application' do
    name 'name'
    redirect_uri 'urn:ietf:wg:oauth:2.0:oob'
  end
end
