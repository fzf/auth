require 'rails_helper'

RSpec.describe 'authenticate' do
  let(:user) { FactoryBot.create :user }
  let!(:doorkeeper_application) { FactoryBot.create :doorkeeper_application }

  before do
    user.create_direct_otp
  end

  it 'sends a code' do
    headers = {
      "Content-Type" => "application/json"
    }
    post '/users/send_code', params: {
      user: {
        phone: user.phone
      },
      authorization: {
        client_id: doorkeeper_application.uid,
        redirect_uri: doorkeeper_application.redirect_uri,
        response_type: 'code'
      }
    }.to_json, headers: headers

  end

  it 'signs in' do
    headers = {
      "Accept" => 'application/json',
      "Content-Type" => "application/json"
    }
    post '/users/sign_in', params: {
      user: {
        phone: user.phone,
        password: user.direct_otp
      },
      client_id: doorkeeper_application.uid,
      redirect_uri: doorkeeper_application.redirect_uri,
      response_type: 'code'
    }.to_json, headers: headers

    auth_code = JSON.parse(response.body).deep_symbolize_keys[:redirect_uri][:code]

    post '/oauth/token', params: {
      grant_type: 'authorization_code',
      code: auth_code,
      redirect_uri: doorkeeper_application.redirect_uri,
      client_id: doorkeeper_application.uid,
      client_secret: doorkeeper_application.secret
    }.to_json, headers: headers

    expect(JSON.parse(response.body)['access_token']).to be
  end

  it 'signs in with PKCE' do
    code_verifier = 'a'*128
    code_challenge = Base64.urlsafe_encode64(Digest::SHA256.digest(code_verifier)).split('=')[0]

    headers = {
      "Content-Type" => "application/json"
    }
    post '/users/sign_in', params: {
      user: {
        phone: user.phone,
        password: user.direct_otp
      },
      client_id: doorkeeper_application.uid,
      redirect_uri: doorkeeper_application.redirect_uri,
      response_type: 'code',
      code_challenge: code_challenge,
      code_challenge_method: 'S256'
    }.to_json, headers: headers

    auth_code = JSON.parse(response.body).deep_symbolize_keys[:redirect_uri][:code]

    post '/oauth/token', params: {
      grant_type: 'authorization_code',
      code: auth_code,
      redirect_uri: doorkeeper_application.redirect_uri,
      client_id: doorkeeper_application.uid,
      client_secret: doorkeeper_application.secret,
      code_verifier: code_verifier,
      code_challenge_method: 'S256'
    }.to_json, headers: headers

    expect(JSON.parse(response.body)['access_token']).to be
  end

  it 'fails sign in' do
    headers = {
      "Content-Type" => "application/json"
    }
    post '/users/sign_in', params: {
      user: {
        phone: user.phone,
        password: '000000'
      },
      authorization: {
        client_id: doorkeeper_application.uid,
        redirect_uri: doorkeeper_application.redirect_uri,
        response_type: 'code'
      }
    }.to_json, headers: headers

    expect(
      JSON.parse(response.body).deep_symbolize_keys[:errors]
    ).to eq(
      [
        {
          status: '401',
          title: 'Unauthorized'
        }
      ]
    )
  end
end
