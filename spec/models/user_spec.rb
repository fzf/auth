require 'rails_helper'

RSpec.describe User, type: :model do
  it{
    should have_many(
      :access_grants
    ).class_name(
      "Doorkeeper::AccessGrant"
    ).with_foreign_key(
      :resource_owner_id
    ).dependent(
      :destroy
    )
  }

  it {
    should have_many(
      :access_tokens
    ).class_name(
      "Doorkeeper::AccessToken"
    ).with_foreign_key(
      :resource_owner_id
    ).dependent(
      :destroy
    )
  }
end
