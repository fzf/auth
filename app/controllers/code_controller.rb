class CodeController < ApplicationController
  def send_code
    user = User.find_or_create_by(phone: params[:phone])
    user.send_new_otp

    render json: {code: user.direct_otp}, status: :ok
  end
end
