class ApplicationController < ActionController::Base
  respond_to :json, :html
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  def home
  end

protected

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || stored_location_for(resource) || root_path
  end
end
