class Users::SessionsController < Devise::SessionsController
  include Doorkeeper::Helpers::Controller

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)
    redirect_or_render authorize_response
  end

  private

  def redirect_or_render(auth)
    if auth.redirectable?
      if Doorkeeper.configuration.api_only
        render(
          json: { status: :redirect, redirect_uri: auth.redirect_uri },
          status: auth.status
        )
      else
        redirect_to auth.redirect_uri
      end
    else
      render json: auth.body, status: auth.status
    end
  end

  def pre_auth
    @pre_auth ||= Doorkeeper::OAuth::PreAuthorization.new(Doorkeeper.configuration,
                                              server.client_via_uid,
                                              params)
  end

  def authorization
    @authorization ||= strategy.request
  end

  def strategy

    @strategy ||= server.authorization_request pre_auth.response_type
  end

  def authorize_response
    @authorize_response ||= begin
      authorizable = pre_auth.authorizable?
      before_successful_authorization if authorizable
      auth = strategy.authorize
      after_successful_authorization if authorizable
      auth
    end
  end

  def after_successful_authorization
    Doorkeeper.configuration.after_successful_authorization.call(self)
  end

  def before_successful_authorization
    Doorkeeper.configuration.before_successful_authorization.call(self)
  end
end
