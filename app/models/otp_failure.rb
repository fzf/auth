class OtpFailure < Devise::FailureApp
  def respond
    self.status = 401
    self.content_type = 'application/json'
    self.response_body = error_body.to_json
  end

  def error_body
    {
      errors: [
        {
          status: '401',
          title: 'Unauthorized'
        }
      ]
    }
  end
end
