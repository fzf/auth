require_relative 'devise/strategies/one_time_authenticatable'

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :two_factor_authenticatable, :database_authenticatable, :registerable,
         authentication_keys: [:phone]

  has_many :access_grants,
    class_name: "Doorkeeper::AccessGrant",
    foreign_key: :resource_owner_id,
    dependent: :destroy

  has_many :access_tokens,
    class_name: "Doorkeeper::AccessToken",
    foreign_key: :resource_owner_id,
    dependent: :destroy

  has_one_time_password(encrypted: true)

  validates :phone, length: { is: 10 }, numericality: { only_integer: true }

  def send_two_factor_authentication_code(code)
    twilio.api.account.messages.create(
      from: ENV['FROM_NUMBER'],
      to: phone,
      body: "Your auth code: #{code}"
    ) unless ENV['DONT_SEND_2FA_CODE']
  end

  def need_two_factor_authentication?(request)
    false
  end

  def twilio
    @client = Twilio::REST::Client.new ENV['ACCOUNT_SID'], ENV['AUTH_TOKEN']
  end
end
