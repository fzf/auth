# frozen_string_literal: true

require 'devise/strategies/one_time_authenticatable'

module Devise
  module Models
    module OneTimeAuthenticatable
    end
  end
end
